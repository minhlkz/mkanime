var mongoose = require('mongoose');
var episodeSchema = mongoose.Schema({
    coverImage: String,
    title: String,
    status: Boolean,
    description: String,
    genres: [String],
    episodes: [{
        title: String,
        url: String
    }]
});
Episode = mongoose.model('episode', episodeSchema);
module.exports = Episode;