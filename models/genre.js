var mongoose = require('mongoose');
var genreSchema = mongoose.Schema({
    coverImage: String,
    Tag: String,
    Description: String, 
});
Genre = mongoose.model('genre', genreSchema);
module.exports = Genre;