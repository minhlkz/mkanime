var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var userSchema = mongoose.Schema({
    userName: String,
    Password: String,
    Email : String,

});
    User = mongoose.model('user', userSchema);

module.exports = User;
module.exports.addUser = function(newUser,callback){
    bcrypt.hash(newUser.Password, 10, function (err, hash) {
        // Store hash in your password DB.
        if(err) throw Error;
        newUser.Password = hash;
        newUser.save(callback);
    });
}
module.exports.getUserByName = (userName , callback) =>{
    User.findOne({userName : userName}, callback);
}
module.exports.comparePassword = (Password,hash,callback) =>{
    bcrypt.compare(Password, hash, function (err, res) {
       if(err) throw err;
       callback(null,res);

    });
}