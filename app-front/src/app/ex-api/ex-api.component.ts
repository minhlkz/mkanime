import { Component, OnInit } from '@angular/core';
import { Episode } from '../models/episode';
import { EpisodeService } from '../services/episode.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-ex-api',
  templateUrl: './ex-api.component.html',
  styleUrls: ['./ex-api.component.scss']
})
export class ExApiComponent implements OnInit {

  episode$: Episode[] = [];
  Info: Episode = new Episode();
  currentPage = 1;
  constructor(private service: EpisodeService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this.loadPage(this.currentPage);
  }
  loadMore() {
    this.currentPage = this.currentPage + 1;
    this.loadPage(this.currentPage);
  }
  showInfo(Ep: Episode) {
    this.Info = Ep;
  }
  loadPage(page: number) {
    this.service.getEpisodesExternalApi(page).subscribe(rs => {
      var data = JSON.parse(rs);
      if (data.success) {
        // this.episode$ = data.episode;
        var data = data.data.data.Page;
        // if (!this.pageInfo$) {
        //   this.pageInfo$ = data.pageInfo;
        // }

        data.media.forEach(e => {
          this.episode$.push(new Episode({
            _id: e._id,
            coverImage: e.coverImage.large,
            title: e.title.romaji,
            status: e.status,
            description: e.description,
            genres: e.genres
          }))
        });

      }
      else {
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
      }




    });
  }

}
