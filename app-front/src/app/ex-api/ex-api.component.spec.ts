import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExApiComponent } from './ex-api.component';

describe('ExApiComponent', () => {
  let component: ExApiComponent;
  let fixture: ComponentFixture<ExApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
