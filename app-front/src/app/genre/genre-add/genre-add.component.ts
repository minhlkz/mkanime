import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Genre } from "../../models/genre";
@Component({
  selector: 'app-genre-add',
  templateUrl: './genre-add.component.html',
  styleUrls: ['./genre-add.component.css']
})
export class GenreAddComponent implements OnInit {
  @Input() genre : Genre;
  @Output() save = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  Save(){
    this.save.emit(this.genre);
  }
}
