import { Component, OnInit } from '@angular/core';
import { GenreService } from '../services/genre.service';
import { Genre } from '../models/genre';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', visibility: 'hidden' })),
      state('expanded', style({ height: '*', visibility: 'visible' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class GenreComponent implements OnInit {
  displayedColumns = ['Tag', 'coverImage', 'Description', 'delete'];
  genre: Genre;
  genre$: Genre[];
  rows = [];
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;
  constructor(private service: GenreService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {

    this.refresh();
  }
  refresh() {
    this.service.getGenres().subscribe(rs => {
      var data = JSON.parse(rs);
      if (data.success) {
        this.genre$ = data.genre;
        this.rows = [];
        this.genre$.forEach(element => this.rows.push(element, { _id: element._id, detailRow: true, element }));
        this.genre$ = this.rows;
        this.genre = null;
      }
      else
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
    });
  }
  openAdd() {
    if (!this.genre)
      this.genre = new Genre();
    else
      this.genre = null;
  }
  Delete(_id: string) {
    this.service.Delete(_id).subscribe(rs => {
      var data = JSON.parse(rs);
      if (data.success) {
        var ele = document.getElementById('resultDelete') as HTMLElement;
        ele.setAttribute('value', '1');
        ele.click();
        var i;
        this.genre$ = this.genre$.filter(val => {
          return val._id.toString() !== _id;
        });
      }
      else {
        var ele = document.getElementById('resultDelete') as HTMLElement;
        ele.setAttribute('value', '0');
        ele.click();
      }
    });
  }
  Update(_id: any, genre: Genre) {
    genre._id = _id;
    this.service.Update(genre).subscribe(rs => {
      var data = JSON.parse(rs);
      if (data.success)
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-success text-center ', timeout: 2000 });
      else
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
    });
  }
  Save(event) {

    this.service.save(event).subscribe(rs => {
      var data = JSON.parse(rs);
      if (data.success) {
        this.refresh();
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-success text-center ', timeout: 2000 });
      }
      else
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
    });
  }
}
