import { Component, OnInit } from '@angular/core';
import { EpisodeService } from '../services/episode.service';
import { Episode } from '../models/episode';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-episode',
  templateUrl: './episode.component.html',
  styleUrls: ['./episode.component.css']
})
export class EpisodeComponent implements OnInit {
  displayedColumns = ['title','genres','description','status','edit','delete'];
  episode$ : Episode[] = [];
  currentPage = 0;
  constructor(private service: EpisodeService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
  
    this.service.currentMessage.subscribe(rs => {
      if (rs != "") {
        let data = JSON.parse(rs);
        if (data.success) {
          
          if (data.type == 0) {
            //INSERT
            this.episode$.push(data.episode);
          }
          else {
            //UPDATE type == 1
            this.episode$ = this.episode$.map(e => {
              if (e._id == data.episode._id) {
                return data.episode;
              }
              return e;
            });
          }
        }
      }
    });
    this.loadMore();
    
  }
  loadMore() {
    this.currentPage = this.currentPage + 1;
    this.loadPage(this.currentPage);
  }
  loadPage(page : number){
    this.service.getEpisodes(page).subscribe(rs => {
      var data = JSON.parse(rs);
      if (data.success) {
        this.episode$.push(...data.episode);
      }
      else {
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
      }
    });
  }
  Delete(_id : string){
    this.service.Delete(_id).subscribe(rs => {
      var data = JSON.parse(rs);
      if (data.success) {
        var ele = document.getElementById('resultDelete') as HTMLElement;
        ele.setAttribute('value', '1');
        ele.click();
        var i;
        this.episode$ = this.episode$.filter(val => {
          return val._id.toString() !== _id;
        });
      }
      else {
        var ele = document.getElementById('resultDelete') as HTMLElement;
        ele.setAttribute('value', '0');
        ele.click();
      }


    })
  }
}