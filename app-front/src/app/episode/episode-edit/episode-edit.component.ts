import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Episode } from '../../models/episode';
import { EpisodeService } from '../../services/episode.service';
import { FormControl } from '@angular/forms';
import { GenreService } from '../../services/genre.service';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-episode-edit',
  templateUrl: './episode-edit.component.html',
  styleUrls: ['./episode-edit.component.css']
})
export class EpisodeEditComponent implements OnInit {
  episode$ = new Episode();
  // toppings = new FormControl();
  genres$ = [];
  constructor(private route: ActivatedRoute, private service: EpisodeService, private genreService: GenreService, private _flashMessagesService: FlashMessagesService,private router : Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(param => {
      this.service.getEpisode(param._id).subscribe(rs => {

        var data = JSON.parse(rs);
        if (data.success) {
          this.episode$ = data.episode;
        }
        else {
          this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
        }
      }
      );
      //TODO : Edit genre
      this.genreService.getGenres().subscribe(rs => {
        var data = JSON.parse(rs);
        if (data.success) {
         data.genre.forEach(i => {
            this.genres$.push(i.Tag);
          });
        }
        else
          this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });


      });

    }
    );
  }
  removeAll() {
    this.episode$ = new Episode();
  }
  Save() {
    this.service.Update(this.episode$).subscribe(rs => {
      var data = JSON.parse(rs);
      if (data.success) {
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-success text-center ', timeout: 2000 });
        this.router.navigate(['/episode']);
      }
      else {
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
      }


      //   var ele = document.getElementById('result') as HTMLElement;
      //   ele.setAttribute('value', '1');
      //   ele.click();
      // }, err => {
      //   var ele = document.getElementById('result') as HTMLElement;
      //   ele.setAttribute('value', '0');
      //   ele.click();
    });
    //save here
  }
  indexTracker(index: number, obj: any) {
    return index;
  }
  addEpisode() {
    this.episode$.episodes.push({ title: '', url: '' });

  }
  removeEpisode(i: number) {
    this.episode$.episodes = this.episode$.episodes.filter((val, index) => {
      return index != i ? true : false;
    });

  }
  // addGenre() {
  //   this.episode$.genres.push("");

  // }
  // removeGenre(i: number) {
  //   this.episode$.genres = this.episode$.genres.filter((val, index) => {
  //     return index != i ? true : false;
  //   });

  // }
}
