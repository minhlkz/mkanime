import { Component, OnInit, Input } from '@angular/core';
import { EpisodeService } from "../../services/episode.service";
import { Episode } from '../../models/episode';
import { GenreService } from '../../services/genre.service';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-episode-add',
  templateUrl: './episode-add.component.html',
  styleUrls: ['./episode-add.component.css']
})
export class EpisodeAddComponent implements OnInit {
  episode = new Episode();
  genres$ = [];
  toppings = new FormControl();
  mess : string;
  constructor(private service: EpisodeService, private genreService: GenreService, private router: Router, private _flashMessagesService: FlashMessagesService) { }
  // newMessage() {
  //   this.service.changeMessage("Hello from Sibling" + Math.random()*10)
  // }
  ngOnInit() {
    // this.service.currentMessage.subscribe(mes => {
    //   this.mess = mes;
    // })


      this.genreService.getGenres().subscribe(rs => {
        var data = JSON.parse(rs);
        if (data.success) {
          data.genre.forEach(i => {
            this.genres$.push(i.Tag);
          });
        }
        else
          this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });


      });
  }
  Save() {
    this.service.save(this.episode).subscribe(rs => {
      // this.result = 1;
      var ele = document.getElementById('result') as HTMLElement;
      ele.setAttribute('value', '1');
      ele.click();
      this.router.navigate(['/episode']);
    }, err => {
      var ele = document.getElementById('result') as HTMLElement;
      ele.setAttribute('value', '0');
      ele.click();
    });
    //save here
  }
  removeAll() {
    this.episode = new Episode();
  }
  indexTracker(index: number, obj: any) {
    return index;
  }
  addEpisode() {
    this.episode.episodes.push({ title: '', url: '' });

  }
  removeEpisode(i: number) {
    this.episode.episodes = this.episode.episodes.filter((val, index) => {
      return index != i ? true : false;
    });

  }
  // addGenre() {
  //   this.episode.genres.push("");

  // }
  // removeGenre(i: number) {
  //   this.episode.genres = this.episode.genres.filter((val, index) => {
  //     return index != i ? true : false;
  //   });

  // }
}
