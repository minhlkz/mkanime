import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Episode } from '../models/episode';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { ApiconfigService } from './apiconfig.service';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',

  })

};
@Injectable()
export class EpisodeService {
  private episodeSource = new BehaviorSubject('');
  currentMessage = this.episodeSource.asObservable();
  
  constructor(private http: HttpClient,private config : ApiconfigService) { }

  notifyChange(episode : string) {
    this.episodeSource.next(episode);
  }

  getEpisodes(page: number): any {
    return this.http.get(this.config.ApiHost + 'episode/list?page=' + page, httpOptions).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    );
  }
  getEpisodesExternalApi(page: number) {
    // return this.http.get('episode/list', httpOptions).pipe(
    //   map(rs =>
    //     JSON.stringify(rs)
    //   )
    // );
    var query = `
          query ($page: Int, $perPage: Int) {
            Page (page: $page, perPage: $perPage) {
                  
              pageInfo {
                total
                currentPage
                lastPage
                hasNextPage
                perPage
              }
              media {
                _id : id
                title {
                  romaji
                  english
                  native
                  userPreferred
                }
                genres 
                description
                status
                coverImage {
                  large
                  medium
                }
              }
            }
          }
          `;
    var variables = {
      page: page,
      perPage: 10
    };
    return this.http.post('https://graphql.anilist.co',

      {

        query: query,
        variables: variables

      }
      , {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }),


      }).pipe(map(rs => {
        // console.log(rs)
        return JSON.stringify({ success: true, data: rs })
      }));




  }

  getEpisode(_id: string) {
    return this.http.get(this.config.ApiHost +'episode/get?_id=' + _id, httpOptions).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    );
  }
  save(episode: Episode) {
    return this.http.post(this.config.ApiHost + 'episode/add', episode, httpOptions).pipe(
      map(rs => {
        let ep = JSON.stringify(rs);
        this.notifyChange(ep);
        return ep;
      }
      )
    );
    //handle error ??
  }
  Update(episode: Episode) {
    return this.http.put(this.config.ApiHost +'episode/update', episode, httpOptions).pipe(
      map(rs => {
        let ep = JSON.stringify(rs);
        this.notifyChange(ep);
        return ep;
      })
    );
    //handle error ??
  }
  Delete(_id: string) {
    return this.http.request('delete', this.config.ApiHost +'episode/delete', { body: { _id: _id } }).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    );
  }
}
