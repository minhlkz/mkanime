import { TestBed, inject } from '@angular/core/testing';

import { ApiconfigService } from './apiconfig.service';

describe('ApiconfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiconfigService]
    });
  });

  it('should be created', inject([ApiconfigService], (service: ApiconfigService) => {
    expect(service).toBeTruthy();
  }));
});
