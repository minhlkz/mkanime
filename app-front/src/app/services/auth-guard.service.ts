import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthService  } from './auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, private router: Router,
    private _flashMessagesService: FlashMessagesService) { }

  canActivate() {
    if (this.auth.loggedIn()) {
      return true;
    } else {
      this._flashMessagesService.show('You need to login', { cssClass: 'alert-danger text-center ', timeout: 2000 });
      this.router.navigate(['login']);
      return false;
    }
  }
}