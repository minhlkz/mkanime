import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Genre } from '../models/genre';
import { map } from 'rxjs/operators';
import { ApiconfigService } from './apiconfig.service';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',

  })

};
@Injectable()
export class GenreService {

  constructor(private http: HttpClient, private config: ApiconfigService) { }
  getGenres() {
    return this.http.get(this.config.ApiHost +'genre/list', httpOptions).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    );
  }
  getGenre(_id: string) {
    return this.http.get(this.config.ApiHost +'genre/get?_id=' + _id, httpOptions).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    );
  }
  save(genre: Genre) {
    return this.http.post(this.config.ApiHost +'genre/add', genre, httpOptions).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    );
    //handle error ??
  }
  Update(genre: Genre) {
    return this.http.put(this.config.ApiHost +'genre/update', genre, httpOptions).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    );
    //handle error ??
  }
  Delete(_id: string) {
    return this.http.request('delete', this.config.ApiHost +'genre/delete', { body: { _id: _id } }).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    );
  }
}