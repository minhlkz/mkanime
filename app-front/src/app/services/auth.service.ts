import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/users';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ApiconfigService } from './apiconfig.service';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',

  })
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  Token: String;
  user: User;
  
  constructor(private http: HttpClient, private jwtHelper: JwtHelperService, private config: ApiconfigService) { }
  registerUser(user: User) {
    

    return this.http.post(this.config.ApiHost +'users/register', user, httpOptions).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    )
  }
  loggedIn() {
    const token: string = this.jwtHelper.tokenGetter()
    this.Token = token;
    if (!token) {
      return false
    }
    return this.jwtHelper.isTokenExpired(token);
    // return true;
  }
  loginUser(user: User) {
    return this.http.post(this.config.ApiHost +'users/authenticate', user, httpOptions).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    )
  } 
  getloginUserData() {
    this.Token = this.jwtHelper.tokenGetter();
    return this.http.get(this.config.ApiHost +'users/profile', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization' : this.Token.toString()
      })
    }).pipe(
      map(rs =>
        JSON.stringify(rs)
      )
    );
  }
  validateLoginUser(user: User) {
    if (!user.Password || !user.userName)
      return false;
    return true;
  }

  storeUserData(user, token) {
    localStorage.setItem("id_token", token);
    localStorage.setItem("user", JSON.stringify(user));
    this.Token = token;
    this.user = user;
  }

  logoutUser() {
    localStorage.clear();
    this.Token = null;
    this.user = null;
  }
}
