import { Component, OnInit } from '@angular/core';
import { EpisodeService } from '../services/episode.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Episode } from '../models/episode';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']

})
export class HomeComponent {
  episode$: Episode[] = [];
  Info: Episode = new Episode();
  currentPage = 1;
  rsz: Observable<any>;
  constructor(private service: EpisodeService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this.service.currentMessage.subscribe(rs => {
      if (rs != "") {
        let data = JSON.parse(rs);
        if (data.success) {
          if (data.type == 0) {
            //INSERT
            this.episode$.push(data.episode);
          }
          else {
            //UPDATE type == 1
            this.episode$ = this.episode$.map(e => {
              if (e._id == data.episode._id) {
                return data.episode;
              }
              return e;
            });
          }
        }
      }
    });
    this.loadPage(this.currentPage);
  }
  loadMore() {
    this.currentPage = this.currentPage + 1;
    this.loadPage(this.currentPage);
  }
  showInfo(Ep: Episode) {
    this.Info = Ep;
  }
  loadPage(page: number) {
    this.service.getEpisodes(page).subscribe(rs => {
      var data = JSON.parse(rs);
      if (data.success) {
        this.episode$.push(...data.episode);


      }
      else {
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
      }




    });
  }
}
