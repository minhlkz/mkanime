import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { User } from "../../models/users";
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';
import { Router } from "@angular/router";
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user$ = new User();
  constructor(private _flashMessagesService: FlashMessagesService
    , private service: UserService
    , private authService: AuthService,
      private router: Router) { }

  ngOnInit() {
    // 
  }
  register() {
    if (!this.service.validateUser(this.user$))
      this._flashMessagesService.show('Please fill all empty fields', { cssClass: 'alert-danger text-center ', timeout: 2000 });
    else if (!this.service.validateEmail(this.user$.Email))
      this._flashMessagesService.show('Wrong email address', { cssClass: 'alert-danger text-center ', timeout: 2000 });
    else
      this.authService.registerUser(this.user$).subscribe(rs => {
        var data = JSON.parse(rs);
        if(data.success){
          this._flashMessagesService.show(data.msg, { cssClass: 'alert-success text-center ', timeout: 2000 });
          this.router.navigate(['/login']);
        }
        else{
          this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
          this.router.navigate(['/register']);
        }
      });


  }
}
