import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { User } from "../../models/users";
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user$ = new User();
  constructor(private _flashMessagesService: FlashMessagesService
    , private authService: AuthService,
    private router : Router
  ) { }

  ngOnInit() {
    // 
  }
  login() {
    if (!this.authService.validateLoginUser(this.user$))
      this._flashMessagesService.show('Please fill all empty fields', { cssClass: 'alert-danger text-center ', timeout: 2000 });
    else {
      this.authService.loginUser(this.user$).subscribe(rs => {
        var data = JSON.parse(rs);
        if (data.success) {
          this._flashMessagesService.show(data.msg, { cssClass: 'alert-success text-center ', timeout: 2000 });
          //store to localstorage
          this.authService.storeUserData(data.User,data.Token);
          this.router.navigate(['/']);

        }
        else {
          this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
        }

      });
    }
  }

}
