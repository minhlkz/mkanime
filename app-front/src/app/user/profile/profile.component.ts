import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from '../../models/users';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user$ : User;
  constructor(private authService: AuthService, private _flashMessagesService: FlashMessagesService) { }

  ngOnInit() {
    this.authService.getloginUserData().subscribe(rs => {
      var data = JSON.parse(rs);
      if (data.success) {
        this.user$ = data.user;
        // this._flashMessagesService.show(data.msg, { cssClass: 'alert-success text-center ', timeout: 2000 });
      }
      else
        this._flashMessagesService.show(data.msg, { cssClass: 'alert-danger text-center ', timeout: 2000 });
    })
  }

}
