import { BrowserModule, } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

// NEW MDBOOSTRAP MODULE START
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NavbarModule, WavesModule } from 'angular-bootstrap-md'
// NEW MDBOOSTRAP MODULE END

import { AppComponent } from './app.component';
import { LeftPanelComponent } from './layout/left-panel/left-panel.component';
import { EpisodeComponent } from './episode/episode.component';
import { HttpClientModule } from '@angular/common/http';
import { EpisodeService } from './services/episode.service';
import { GenreService } from './services/genre.service';
import { AuthService } from './services/auth.service';
import { EpisodeEditComponent } from './episode/episode-edit/episode-edit.component';
import { EpisodeAddComponent } from './episode/episode-add/episode-add.component';
import { NavigationComponent } from './layout/navigation/navigation.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatToolbarModule, MatSidenavModule, MatIconModule, MatListModule, MatTableModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatStepperModule } from '@angular/material/stepper';
import { RegisterComponent } from './user/register/register.component';
import { MatCardModule } from '@angular/material/card';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { LoginComponent } from './user/login/login.component';
import { AuthGuard } from "./services/auth-guard.service";
import { ProfileComponent } from './user/profile/profile.component';
import { HomeComponent } from './home/home.component';
import { GenreComponent } from './genre/genre.component';
import { MatSelectModule } from '@angular/material/select';
import { GenreAddComponent } from './genre/genre-add/genre-add.component';
import { MatChipsModule } from '@angular/material/chips';
import { JwtModule } from '@auth0/angular-jwt';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { RouteReuseStrategy } from '@angular/router';
import { CustomReuseStrategy } from './shared/routing';
import { ExApiComponent } from './ex-api/ex-api.component';

export function tokenGetter() {
  return localStorage.getItem('id_token');
}

@NgModule({
  declarations: [
    AppComponent,
    LeftPanelComponent,
    EpisodeComponent,
    EpisodeEditComponent,
    EpisodeAddComponent,
    NavigationComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    HomeComponent,
    GenreComponent,
    GenreAddComponent,
    ExApiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatStepperModule,
    MatCardModule,
    MatSelectModule,
    MatChipsModule,
    FlashMessagesModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    BrowserModule,
    InfiniteScrollModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter

      }
    }),
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
   ,
   
    NavbarModule, WavesModule
  ],
  providers: [
    EpisodeService,
    AuthService,
    GenreService,
    AuthGuard,
    { provide: RouteReuseStrategy, useClass: CustomReuseStrategy },
  ],
  schemas: [NO_ERRORS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
