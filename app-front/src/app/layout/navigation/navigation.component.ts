import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
    public authService: AuthService,
    private router : Router,
    private _flashMessagesService :FlashMessagesService
  ) { }

  logout() {
    this.authService.logoutUser();
    this._flashMessagesService.show('You are now logout', { cssClass: 'alert-danger text-center ', timeout: 2000 });
    this.router.navigate(['/episode']);
  } 

}

