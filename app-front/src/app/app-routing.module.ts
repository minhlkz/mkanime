import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EpisodeComponent } from './episode/episode.component';
import { GenreComponent } from './genre/genre.component';
import { EpisodeEditComponent } from './episode/episode-edit/episode-edit.component';
import { EpisodeAddComponent } from './episode/episode-add/episode-add.component';
import { RegisterComponent } from './user/register/register.component';
import { LoginComponent } from './user/login/login.component';
import { AuthGuard } from "./services/auth-guard.service";
import { ProfileComponent } from "./user/profile/profile.component";
import { HomeComponent } from "./home/home.component";
import { ExApiComponent} from "./ex-api/ex-api.component";
const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
    },
    {
        path: 'exapi',
        component: ExApiComponent,
    },
    {
        path: 'genre',
        component: GenreComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'episode',
        component: EpisodeComponent,
        canActivate: [AuthGuard],
    }, {
        path: 'episode/edit',
        component: EpisodeEditComponent,
        canActivate: [AuthGuard],
    },
    {
        path: 'episode/add',
        component: EpisodeAddComponent,
        canActivate: [AuthGuard],
    }
    ,

    {
        path: 'register',
        component: RegisterComponent,
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard],
    },

];

@NgModule({
    imports: [RouterModule.forRoot(routes), RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }