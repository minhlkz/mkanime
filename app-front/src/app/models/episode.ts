export class Episode {
    _id : number = 0;
    coverImage: string;
    title: string;
    status: Boolean;
    description: string;
    genres: string[] ;
    episodes: {
        title: string,
        url: string 
    }[]
    // constructor(){
    //     this.coverImage =  "";
    //    this.title = "";
    //     this.status =false;
    //     this.description = "";
    //    this.genres = [""];
    //     this.episodes = [{ title: '', url: '' }]
    // }
    public constructor(init?: Partial<Episode>) {
        Object.assign(this, init);
    }
}
