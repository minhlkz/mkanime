var express = require('express');
var router = express.Router();
var Episode = require('../models/episode');
/* GET LIST  */
router.get('/list', function (req, res, next) {
  //By Title
  //TODO : add get by page
  var filter = req.query.title == null ? {} : { title: { $regex: '.*' + req.query.title + '.*' } };
  var page = req.query.page;
  if(page) 
    query = Episode.find(filter).skip(10 * (page - 1)).limit(10);
  else
    query = Episode.find(filter);
  query.exec( (err, rs) => {
    if (err)
      res.send({ msg: 'Fail to find', success: false, episode: null })
    res.send({ msg: 'Find successfully', success: true, episode: rs })
  });


});
/* GET BY ID */
router.get('/get', function (req, res, next) {
  Episode.findById(req.query._id,(err, rs) => {
    if (err)
      res.send({ msg: 'Fail to find', success: false, episode: null })
    res.send({ msg: 'Find successfully', success: true, episode: rs })
  });


});

/* POST ADD. */
router.post('/add', function (req, res, next) {
  var episode = new Episode({
    coverImage: req.body.coverImage,
    title: req.body.title,
    status: req.body.status,
    description: req.body.description,
    genres: req.body.genres,
    episodes: req.body.episodes
  });
  episode.save((err, rs) => {
    if (err)
      res.send({ msg: 'Fail to Add', success: false, episode: null })
    res.send({ msg: 'Add successfully', success: true, episode: rs , type : 0})
  });
  // res.send(episode);
});

/* PUT UPDATE. */
router.put('/update', function (req, res, next) {
  var episode = new Episode({
    _id: req.body._id,
    coverImage: req.body.coverImage,
    title: req.body.title,
    status: req.body.status,
    description: req.body.description,
    genres: req.body.genres,
    episodes: req.body.episodes
  });
  Episode.update({ _id: episode._id }, episode, (err, rs) => {
    if (err)
      res.send({ msg: 'Fail to update', success: false})
    res.send({ msg: 'Update successfully', success: true ,type : 1 , episode : episode})
  });
});

/* DELETE REMOVE. */
router.delete('/delete', function (req, res, next) {
  Episode.deleteOne({ _id: req.body._id }, (err) => {
    if (err)
      res.send({ msg: 'Fail to delete', success: false })
    res.send({ msg: 'Delete successfully', success: true })
  });
  // res.send({ success: 'success' });
});


module.exports = router;
