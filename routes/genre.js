var express = require('express');
var router = express.Router();
var Genre = require('../models/genre');
var Episode = require('../models/episode');
/* GET LIST  */
router.get('/list', function (req, res, next) {
  //By Tag
  var filter = req.query.Tag == null ? {} : { Tag: { $regex: '.*' + req.query.Tag + '.*' } };
  // Genre.find(filter, (err, rs) => {
  //   if (err)
  //     res.send({ msg: 'Fail to find', success: false, genre: null })
  //   res.send({ msg: 'Find successfully', success: true, genre: rs })
  // });
  Genre.find(filter).exec( (err, rs) => {
    if (err)
      res.send({ msg: 'Fail to find', success: false, genre: null })
    res.send({ msg: 'Find successfully', success: true, genre: rs })
  })
  
  ;


});
/* GET BY ID */
router.get('/get', function (req, res, next) {
  Genre.findById(req.query._id, (err, rs) => {
    if (err)
      res.send({ msg: 'Fail to find', success: false, genre: null })
    res.send({ msg: 'Find successfully', success: true, genre: rs })
  });


});

/* POST ADD. */
router.post('/add', function (req, res, next) {
  var genre = new Genre({
    coverImage: req.body.coverImage,
    Tag: req.body.Tag,
    Description: req.body.Description,
  });
  genre.save((err, rs) => {
    if (err) 
     res.send({ msg: 'Fail to Add', success: false, genre: null })
    res.send({msg : 'Add successfully' , success : true, genre : rs})
  });
});

/* PUT UPDATE. */
router.put('/update', function (req, res, next) {
  var genre = new Genre({
    _id: req.body._id,
    coverImage: req.body.coverImage,
    Tag: req.body.Tag,
    Description: req.body.Description,
  });
  Genre.findOneAndUpdate({ _id: genre._id }, genre, (err, rs) => {
    if (err) res.send({ msg: 'fail to update', success: false });
    Episode.find({ genres: rs.Tag },(err,episodes)=>{
      if(err) throw err;
      episodes.forEach(episode => {
        let i = episode.genres.indexOf(rs.Tag);
        
        if (i > -1) {
          episode.genres.splice(i, 1);
          episode.genres.push(genre.Tag);
        }
        episode.save((err, rs) => {
          if (err) throw err;
        })
      })
    });
  });
  res.send({ msg: 'update successfully', success: true });
});

/* DELETE REMOVE. */
router.delete('/delete', function (req, res, next) {
  Genre.deleteOne({ _id: req.body._id }, (err) => {
    if (err) {
      res.send({ msg: 'fail to delete', success: false });
    }
  });
  res.send({ msg: 'delete successfully', success: true });
});


module.exports = router;
