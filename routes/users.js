var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/user');

const jwt = require('jsonwebtoken');
const passport = require("passport");

var env = process.env.NODE_ENV || 'development';
var config = require('../config/config')[env];
/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});
router.post('/register', (req, res, next) => {
  let newUser = new User({
    userName: req.body.userName,
    Password: req.body.Password,
    Email: req.body.Email
  });
  User.addUser(newUser, (err, rs) => {
    if (err)
      res.json({ msg: "Something went wrong during registration process" ,success: false });
    res.json({  msg : 'Account ' + newUser.userName + ' is created successfully', success : true,user: rs });

   });



});
router.post('/authenticate', (req, res, next) => {
  const userName = req.body.userName;
  User.getUserByName(userName, (err, user) => {
    if (err) throw err;
    if (!user) res.json({ msg: "user doesn't exists" , success : false});
    User.comparePassword(req.body.Password, user.Password, (err, isMatch) => {
      if (err) throw err;
      if (!isMatch) res.json({ msg: "wrong password" ,success : false});
      else {
        // res.json({ msg: "login successful" });
        const token = jwt.sign(JSON.parse(JSON.stringify(user)), config.secretOrKey, { algorithm: 'HS256' }, {
          expiresIn: 604000
        });
        res.json({
          Token: 'JWT ' + token
          // Token: token
          ,
          User: {
            userName: user.userName,
            Password: user.Password,
            Email: user.Email,
            
          }
          ,msg : "Login successfully"
          ,success : true
        });



      }
    });
  });
  

});
router.get('/profile', passport.authenticate('jwt',{session : false}), (req, res, next) => {
  res.json({ msg: "", success: true, user: req.user });
});
module.exports = router;
