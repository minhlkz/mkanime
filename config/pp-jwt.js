const passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
var User = require('../models/user');
var env = process.env.NODE_ENV || 'development';
var config = require('../config/config')[env];
passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
    secretOrKey: config.secretOrKey
}

    , (jwt_payload, done) => {

        User.findOne({ _id: jwt_payload._id }, function (err, user) {
            if (err) {
              
                return done(err, false);
            }
            if (user) {
                return done(null, user);
            } else {
                return done('null', false);
                // or you could create a new account
            }
        });
    }));
// }