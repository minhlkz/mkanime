var config = {
development: {
    //url to be used in link generation
    url: 'http://my.site.com',
    //mongodb connection settings
    database: {
        host:   '127.0.0.1',
        port:   '27017',
        db:     'site_dev'
    },
    //server details
    server: {
        host: '127.0.0.1',
        port: '3000'
    },
    secretOrKey : 'secret'
},
production: {
    //url to be used in link generation
    url: 'http://my.site.com',
    //mongodb connection settings
    database: {
        host: 'minhlk:minhlk26071996@ds125211.mlab.com',
        port: '25211',
        db: 'mk-manga-db'
    },
    //server details
    server: {
        host:   '',
        port:   '8080'
    },
    secretOrKey: 'secret'
}
};
module.exports = config;